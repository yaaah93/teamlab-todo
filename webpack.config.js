const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = [
	{
		context: path.join(__dirname, 'src', 'javascripts'),
		entry: { app: './app.js' },
		output: {
			path: path.join(__dirname, 'public', 'javascripts'),
			filename: '[name].js'
		},
		module: {
			rules: [
				{
					test: /\.vue$/,
					exclude: /node_modules/,
					loader: 'vue-loader'
				},
				{
					test: /\.js$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: ['es2015', 'stage-2'],
							plugins: ['transform-runtime']
						}
					}
				}
			]
		},
		devtool: 'inline-source-map',
		plugins: [
			new webpack.DefinePlugin({
				'process.env': {
					NODE_ENV: '"production"'
				}
			})
		]
	},
	{
		context: path.join(__dirname, 'src', 'sass'),
		entry: { style: './style.scss' },
		output: {
			path: path.join(__dirname, 'public', 'css'),
			filename: '[name].css'
		},
		module: {
			rules: [{
				test: /\.scss$/,
				exclude: /node_modules/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						{
							loader: 'css-loader',
							options: {
								url: false
							}
						},
						'sass-loader'
					]
				})
			}]
		},
		devtool: 'inline-source-map',
		plugins: [
			new ExtractTextPlugin({
				filename: '[name].css'
			})
		]
	}
];
