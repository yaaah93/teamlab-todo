import Vue from 'vue';

import routes from './routes';
import store from './store';

import App from './layouts/App.vue';

const app = new Vue({
	el: '#app',
	router: routes,
	store: store,
	render: (h) => h(App)
});
