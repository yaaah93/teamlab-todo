import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import Home from './pages/Home.vue';
import Search from './pages/Search.vue';
import Todo from './pages/Todo.vue';
import NotFound from './pages/404.vue';

export default new VueRouter({
	routes: [
		{ path: '/', component: Home },
		{ path: '/search', component: Search },
		{ path: '/todo/:id', component: Todo },
		{ path: '*', component: NotFound }
	]
});
