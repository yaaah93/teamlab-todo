import moment from 'moment';

import TodolistsAPI from '../api/todolists';

const todo = {
	namespaced: true,
	state: {
		inbox: {
			expired: { term: {}, todos: []},
			today: { term: {}, todos: []},
			tommorow: { term: {}, todos: []},
			within: { term: {}, todos: []},
		},
		todolist: {},
		todos: [],
		errors: [],
		edit_id: null
	},
	actions: {
		getInboxTodos: ({commit}) => {
			TodolistsAPI.getInboxTodos().then(res => {
				commit('setInbox', res.data);
			});
		},
		getSearchTodos: ({commit}, q) => {
			TodolistsAPI.getSearchTodos(q).then(res => {
				commit('setTodos', res.data);
			});
		},
		getTodos: ({commit}, todolistId) => {
			TodolistsAPI.getTodos(todolistId).then(res => {
				commit('setTodolist', res.todolist)
				commit('setTodos', res.todos);
			});
		},
		editTodo: ({commit}, id) => {
			commit('editTodo', id);
		},
		cancelTodo: ({commit}) => {
			commit('editTodo', null);
		},
		addTodo: ({commit}, [todo, todolistId]) => {
			if (todo._id) {
				return new Promise((resolve, reject) => {
					TodolistsAPI.modifyTodo(todo, todolistId).then(() => {
						resolve();
					}).catch(err => {
						commit('setErrorsTodo', err);
					});
				});
			} else {
				return new Promise((resolve, reject) => {
					TodolistsAPI.addTodo(todo, todolistId).then(() => {
						resolve();
					}).catch(err => {
						commit('setErrorsTodo', err);
					});
				});
			}
		},
		removeTodo: ({commit}, [id, todolistId]) => {
			return new Promise((resolve, reject) => {
				TodolistsAPI.removeTodo(id, todolistId).then(res => {
					resolve();
				});
			});
		},
		finishedTodo: ({commit}, [id, todolistId]) => {
			return new Promise((resolve, reject) => {
				TodolistsAPI.finishedTodo(id, todolistId).then(res => {
					resolve();
				});
			});
		}
	},
	getters: {
		inbox: (state) => state.inbox,
		todolist: (state) => state.todolist,
		todos: (state) => state.todos,
		editTodo: (state) => state.edit_id,
		errorsTodo: (state) => state.errors
	},
	mutations: {
		setInbox: (state, todos) => {
			state.errors = null;
			state.edit_id = null;
			state.inbox = todos;
		},
		setTodolist: (state, todolist) => {
			state.todolist = todolist;
		},
		setTodos: (state, todos) => {
			state.errors = null;
			state.edit_id = null;
			state.todos = todos;
		},
		editTodo: (state, id) => {
			state.errors = null;
			state.edit_id = id;
		},
		setErrorsTodo: (state, errors) => {
			state.errors = errors;
		}
	}
}

export default todo;