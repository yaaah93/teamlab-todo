import moment from 'moment';

import TodolistsAPI from '../api/todolists';

const todolist = {
	namespaced: true,
	state: {
		todolists: [],
		errors: [],
		edit_id: null
	},
	actions: {
		getTodolists: ({commit}) => {
			TodolistsAPI.getTodolists().then(res => {
				commit('setTodolists', res.data);
			});
		},
		editTodolist: ({commit}, id) => {
			commit('editTodolist', id);
		},
		cancelTodolist: ({commit}) => {
			commit('editTodolist', null);
		},
		addTodolist: ({commit}, todolist) => {
			let cb = res => {
				TodolistsAPI.getTodolists().then(res => {
					commit('setTodolists', res.data);
				});
			};
			let err = res => {
				commit('setErrorsTodolist', res);
			};
			if (todolist._id) {
				return TodolistsAPI.modifyTodolist(todolist).then(cb).catch(err);
			} else {
				return TodolistsAPI.addTodolist(todolist).then(cb).catch(err);
			}
		},
		removeTodolist: ({commit}, id) => {
			TodolistsAPI.removeTodolist(id).then(res => {
				TodolistsAPI.getTodolists().then(res => {
					commit('setTodolists', res.data);
				});
			});
		}
	},
	getters: {
		todolists: (state) => {
			let todolists = state.todolists.map(todolist => {
				// 完了したTodoを取得
				let todos_complete = todolist.todos.filter(todo => todo.finishedAt != null);
				todolist.todos_complete = todos_complete ? todos_complete : [];
				// 期限の近い日時を取得
				todolist.todo_limitAt = (() => {
					let limitAt = null;
					todolist.todos.forEach(todo => {
						if (todo.finishedAt == null) {
							if (limitAt == null) limitAt = todo.limitAt;
							if (moment(todo.limitAt).isBefore(limitAt)) {
								limitAt = todo.limitAt;
							}
						}
					});
					return limitAt;
				})();
				return todolist;
			});
			return todolists;
		},
		editTodolist: (state) => state.edit_id,
		errorsTodolist: (state) => state.errors
	},
	mutations: {
		setTodolists: (state, todolists) => {
			state.errors = null;
			state.edit_id = null;
			state.todolists = todolists;
		},
		editTodolist: (state, id) => {
			state.errors = null;
			state.edit_id = id;
		},
		setErrorsTodolist: (state, errors) => {
			state.errors = errors;
		}
	}
}

export default todolist;
