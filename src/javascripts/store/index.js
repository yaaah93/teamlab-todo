import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

import todolist from './todolist';
import todo from './todo';

const store = new Vuex.Store({
	modules: {
		todolist,
		todo
	}
});

export default store;