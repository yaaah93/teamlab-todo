import axios from 'axios';

const API_URI = '/api/todolists';

const saveCallback = res => {
	let data = res.data;
	if (!data.errors) {
		return data;
	} else {
		let errorMessages = Object.keys(data.errors).map(key => {
			return data.errors[key].message;
		});
		return Promise.reject(errorMessages);
	}
};

export default {
	getTodolists(cb) {
		return axios.get(API_URI);
	},
	addTodolist(todolist, cb) {
		return axios.post(API_URI, todolist)
			.then(saveCallback);
	},
	modifyTodolist(todolist, cb) {
		return axios.put(`${API_URI}/${todolist._id}`, todolist)
			.then(saveCallback);
	},
	removeTodolist(id, cb) {
		return axios.delete(`${API_URI}/${id}`);
	},
	getInboxTodos(cb) {
		return axios.get(`${API_URI}/todos`);
	},
	getSearchTodos(q, cb) {
		return axios.get(`${API_URI}/search?q=${q}`);
	},
	getTodos(todolistId, cb) {
		return Promise.all([
			axios.get(`${API_URI}/${todolistId}/todos`),
			axios.get(`${API_URI}/${todolistId}`)
		]).then(([res_todos, res_todolist]) => {
			return {
				todos: res_todos.data,
				todolist: res_todolist.data
			};
		});
	},
	addTodo(todo, todolistId, cb) {
		return axios.post(`${API_URI}/${todolistId}/todos`, todo)
			.then(saveCallback);
	},
	modifyTodo(todo, todolistId, cb) {
		return axios.put(`${API_URI}/${todolistId}/todos/${todo._id}`, todo)
			.then(saveCallback);
	},
	removeTodo(id, todolistId, cb) {
		return axios.delete(`${API_URI}/${todolistId}/todos/${id}`);
	},
	finishedTodo(id, todolistId, cb) {
		return axios.put(`${API_URI}/${todolistId}/todos/${id}/finished`)
			.then(saveCallback);
	}
}