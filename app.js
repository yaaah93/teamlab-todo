const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const ECT = require('ect');

const index = require('./routes/index.js');
const todolists = require('./routes/api/todolists.js');

const app = express();
const port = process.env.PORT || '3000';
app.listen(port);

// Database
const mongoose = require('mongoose');
const mongo_uri = process.env.MONGODB_URI ? process.env.MONGODB_URI : 'mongodb://teamlabtodo_mongo_1/todo';
mongoose.Promise = global.Promise;
mongoose.connect(mongo_uri, { useMongoClient: true });

// Views
app.set('views', path.join(__dirname, 'views'));
app.engine('ect', ECT({ watch: true, root: path.join(__dirname, 'views'), ext: '.ect' }).render);
app.set('view engine', 'ect');

// Public
app.use(express.static(path.join(__dirname, 'public')));

// Json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Router
app.use('/', index);

app.use('/api/todolists', todolists);