const express = require('express');
const router = express.Router();

const moment = require('moment');
const async = require('async');

// Model
const Todolist = require('../../models/todolist');
const Todo = require('../../models/todo');

// /api/todolistsへのアクセス
router.route('/')
	// すべてのTodoリストを取得
	.get((req, res) => {
		let q = req.query.q;
		let where = {};
		if (q) {
			where.title = new RegExp(`.*${q}.*`, "i");
		}
		Todolist.find(where).populate('todos')
			.then(todolist => {
				res.json(todolist);
			})
			.catch(err => {
				res.send(err);
			});
	})
	// Todoリストの作成
	.post((req, res) => {
		let todolist = new Todolist({
			title: req.body.title,
			color: req.body.color
		});
		todolist.save()
			.then(todolist => {
				res.json({});
			})
			.catch(err => {
				res.json(err);
			});
	});

// /api/todolists/todosへのアクセス
router.route('/todos')
	// 期限切れ・今日・明日・5日以内のTODOを取得
	.get((req, res) => {
		let terms = {
			expired: [null, moment().add(-1, 'day')],
			today: [moment(), moment()],
			tommorow: [moment().add(1, 'day'), moment().add(1, 'day')],
			within: [moment().add(2, 'day'), moment().add(7, 'day')]
		};
		// 検索対象の開始・終了日時を算出
		let get_term = ([fromAt, toAt]) => {
			fromAt = fromAt ? moment(fromAt).format('YYYY-MM-DD') : null;
			toAt = toAt ? moment(toAt).format('YYYY-MM-DD') : null;
			return  {
				fromAt: fromAt ? moment(fromAt) : null,
				toAt: toAt ? moment(toAt).add(1, 'day').add(-1, 'second') : null
			};
		};
		// 検索対象期間をループして検索・取得
		let inbox = {};
		async.forEach(Object.keys(terms), (key, cb) => {
			let term = get_term(terms[key]);
			let where = { 'finishedAt' : null, 'limitAt' : {} };
			if (term.fromAt != null) {
				where['limitAt']['$gte'] = term.fromAt.format();
			}
			if (term.toAt != null) {
				where['limitAt']['$lte'] = term.toAt.format();
			}
			Todo.find(where)
				.sort({ createdAt: 1 })
				.populate('_todolist')
				.exec((err, todos) => {
					inbox[key] = {
						term: term,
						todos: todos
					};
					cb();
				});
		}, err => {
			res.json(inbox)
		});
	});

// /api/todolists/search?q=xxxへのアクセス
router.route('/search')
	// TODOのタイトルを検索し取得
	.get((req, res) => {
		let q = req.param('q');
		if (q) {
			Todo.find({ title: new RegExp(`.*${q}.*`, 'i') })
				.sort({ createdAt: 1 })
				.populate('_todolist')
				.exec((err, todos) => {
					res.json(todos);
				});
		} else {
			res.json([]);
		}
	});

// /api/todolists/:todolist_idへのアクセス
router.route('/:todolist_id')
	// IDに該当するTodoリストを取得
	.get((req, res) => {
		Todolist.findById(req.params.todolist_id).populate('todos')
			.then(todolist => {
				res.json(todolist);
			})
			.catch(err => {
				res.json(err);
			});
	})
	// IDに該当するTodoリストを更新
	.put((req, res) => {
		let modify = {
			title: req.body.title,
			color: req.body.color
		};
		Todolist.findById(req.params.todolist_id)
			.then(todolist => {
				todolist.title = req.body.title;
				todolist.color = req.body.color;
				todolist.save()
					.then(todolist => {
						res.json({});
					})
					.catch(err => {
						res.json(err);
					})
			})
			.catch(err => {
				res.json(err);
			})
	})
	// IDに該当するTodoリストを削除
	.delete((req, res) => {
		Todolist.findById(req.params.todolist_id)
			.then(todolist => {
				todolist.remove()
					.then(() => {
						res.json({});
					})
					.catch(err => {
						res.json(err);
					})
			})
			.catch(err => {
				res.json(err);
			});
	});

// /api/todolists/:todolist_id/todosへのアクセス
router.route('/:todolist_id/todos')
	// すべてのTodoを取得
	.get((req, res) => {
		let q = req.query.q;
		let where = {
			_todolist: req.params.todolist_id
		};
		if (q) {
			where.title = new RegExp(`.*${q}.*`, "i");
		}
		Todo.find(where)
			.sort({ createdAt: 1 })
			.populate('_todolist')
			.then(todo => {
				res.json(todo);
			})
			.catch(err => {
				res.json(err);
			});
	})
	// Todoの作成
	.post((req, res) => {
		let todo = new Todo({
			title: req.body.title,
			limitAt: req.body.limitAt,
			finishedAt: null,
			_todolist: req.params.todolist_id
		});
		todo.save()
			.then(todo => {
				res.json({});
			})
			.catch(err => {
				res.json(err);
			});
	});

// /api/todolists/:todolist_id/todos/:todo_idへのアクセス
router.route('/:todolist_id/todos/:todo_id')
	// IDに該当するTodoを取得
	.get((req, res) => {
		Todo.findById(req.params.todo_id).populate('_todolist')
			.then(todo => {
				res.json(todo);
			})
			.catch(err => {
				res.json(err);
			});
	})
	// IDに該当するTodoを更新
	.put((req, res) => {
		Todo.findById(req.params.todo_id)
			.then(todo => {
				todo.title = req.body.title;
				todo.limitAt = req.body.limitAt;
				todo.finishedAt = req.body.finishedAt ? req.body.finishedAt : null;
				todo.save()
					.then(todo => {
						res.json({});
					})
					.catch(err => {
						res.json(err);
					});
			})
			.catch(err => {
				res.json(err);
			});
	})
	// IDに該当するTodoを削除
	.delete((req, res) => {
		Todo.findById(req.params.todo_id)
			.then(todo => {
				todo.remove()
					.then(() => {
						res.json({});
					})
					.catch(err => {
						res.json(err);
					});
			})
			.catch(err => {
				res.json(err);
			});
	});

router.route('/:todolist_id/todos/:todo_id/finished')
	// IDに該当するTodoを完了
	.put((req, res) => {
		Todo.findById(req.params.todo_id)
			.then(todo => {
				if (todo.finishedAt == null) {
					todo.finishedAt = moment().format();
				} else {
					todo.finishedAt = null;
				}
				todo.save()
					.then(todo => {
						res.json({});
					})
					.catch(err => {
						res.json(err);
					});
			})
	});

module.exports = router;