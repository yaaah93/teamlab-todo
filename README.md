TeamLab TODO
====

TODOリストごとに管理ができるTODOアプリケーション
開発言語や環境、起動手順を以下に示します。

## 開発言語・環境

- Node.js v8.1.2
- Express v4.15.3
- Vue.js v2.3.4
- Vue-Router v2.7.0
- Vuex v2.3.1
- MongoDB v3.4.5
- Docker v17.06.0
- Docker-Compose v1.14.0

## 起動及び開発環境のセットアップ

1. NPMライブラリをインストール

	```bash
	$ npm install
	```

2. Dockerイメージの作成

	```bash
	$ docker build -t teamlab-todo:0.1
	```

3. Dockerコンテナの起動

	```bash
	$ docker-compose up
	```

4. webpackの起動とビルド

	```bash
	$ npm run watch
	```

5. http://localhost:3000

## 全体設計及び構成

1. バックエンドにはNode.js + Expressで簡易的なHTTPサーバの構築
2. データベースからの情報を取得し、JsonAPIを出力するように設計
3. データベースはMongoDBを採用
4. TODOリストとTODOのリンクをツリー構造で管理
5. フロントエンドはVue.jsで実装
6. 状態及びデータの管理はVuexを採用
7. Vue Routerでページ遷移を実装
8. CSS設計はFLOCSSをベースに構築

## 独自機能

- トップページを作成し、期限切れ・今日・明日と期限に合わせた表示を実装
- TODOリストを色でも識別できるようカラーピッカーを実装
