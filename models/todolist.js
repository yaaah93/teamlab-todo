const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Todolist = new Schema({
	title: {
		type: String,
		trim: true,
		required: [ true, 'TODOリスト名を入力してください' ],
		maxlength: [ 30, 'TODOリスト名は30文字以内で入力してください' ]
	},
	color: {
		type: String,
		trim: true
	},
	todos: [ { type: Schema.Types.ObjectId, ref: 'Todo' } ]
}, {
	timestamps: true
});

module.exports = mongoose.model('Todolist', Todolist);