const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Todolist = require('./todolist');

const Todo = new Schema({
	_todolist: {
		type: Schema.Types.ObjectId,
		ref: 'Todolist'
	},
	title: {
		type: String,
		trim: true,
		required: [ true, 'Todoを入力してください' ],
		maxlength: [ 30, 'Todoは30文字以内で入力してください' ]
	},
	limitAt: {
		type: Date,
		required: [ true, '期限を入力してください' ]
	},
	finishedAt: {
		type: Date
	}
}, {
	timestamps: true
});

Todo.post('save', (doc, next) => {
	Todolist.update(
		{
			_id: doc._todolist,
			todos: { $nin: [doc._id] }
		},
		{ $push: { todos: doc._id } },
		{ multi: true },
		next
	);
});

Todo.post('remove', (doc, next) => {
	Todolist.update(
		{ todos: doc._id },
		{ $pull: { todos: doc._id } },
		next
	);
});

module.exports = mongoose.model('Todo', Todo);